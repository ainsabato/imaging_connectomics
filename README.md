# imaging_connectomics

Repository for the book chapter "Imaging Connectomics: A new insights for understanding brain diseases".

The chapter text and figures is in chapter folder.

The code to reproduce the figures is in code folder.

The chapter is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.

The code contained in the code folder is licensed under a GPL version 3 license.

See LICENSE.html file for more information.
